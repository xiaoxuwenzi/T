#coding=utf-8
#!/usr/bin/python
import sys
sys.path.append('..') 
from base.spider import Spider
import json
from lxml import html
import requests

class Spider(Spider):
	def getName(self):
		return "Example"
	
	def init(self, extend=""):
		# 初始化代码
		pass
	
	def isVideoFormat(self, url):
		# 检查是否为视频格式的代码
		pass
	
	def manualVideoCheck(self):
		# 手动视频检查代码
		pass
	
	def homeContent(self, filter):
		# 获取主页内容的代码
		result = {}
		# ...existing code...
		result['home_url'] = 'https://www.xinpianchang.com/discover/article-0-0-all-all-0-0-pick'
		return result
	
	def homeVideoContent(self):
		# 获取主页视频内容的代码
		result = {}
		# ...existing code...
		url = 'https://www.xinpianchang.com/discover/article-0-0-all-all-0-0-pick'
		response = requests.get(url)
		tree = html.fromstring(response.content)
		videos = tree.xpath('/html/body/div[1]/section/main/div/div[3]/div[1]/div/div[1]/a[1]')
		video_list = []
		for video in videos:
			video_info = {}
			video_info['url'] = video.xpath('./@href')[0]
			video_info['title'] = video.xpath('./@aria-label')[0]
			style = video.xpath('./div/div/div/@style')[0]
			video_info['cover'] = style.split('url("')[1].split('")')[0]
			video_list.append(video_info)
		result['videos'] = video_list
		return result
	
	def categoryContent(self, tid, pg, filter, extend):
		# 获取分类内容的代码
		result = {}
		# ...existing code...
		if pg == 1:
			result['category_url'] = 'https://www.xinpianchang.com/discover/article-0-0-all-all-0-0-pick'
		else:
			result['category_url'] = f'https://www.xinpianchang.com/discover/article-0-0-all-all-0-0-pick-pp{pg}'
		return result
	
	def detailContent(self, array):
		# 获取详细内容的代码
		result = {}
		# ...existing code...
		return result
	
	def searchContent(self, key, quick):
		# 搜索内容的代码
		result = {}
		# ...existing code...
		return result
	
	def playerContent(self, flag, id, vipFlags):
		# 获取播放器内容的代码
		result = {}
		# ...existing code...
		url = f'https://www.xinpianchang.com/a{id}'
		response = requests.get(url)
		tree = html.fromstring(response.content)
		video_url = tree.xpath('/html/body/div[1]/section/main/section[1]/div[3]/div/div[1]/div[1]/div/div[2]/video/@src')
		if video_url:
			result['url'] = video_url[0]
		return result
	
	config = {
		"player": {},
		"filter": {}
	}
	
	header = {}
	
	def localProxy(self, param):
		# 本地代理的代码
		action = {
			'url':'',
			'header':'',
			'param':'',
			'type':'string',
			'after':''
		}
		return [200, "video/MP2T", action, ""]
